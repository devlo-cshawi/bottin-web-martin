﻿using System;
using System.IO;

namespace Mon_bottin_Web
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lignesBottin;
            string nomFichier = LireEmplacementBottin();

            lignesBottin = LireContenuFichierBottin(nomFichier);
            AfficherBottin(lignesBottin);

            int choixUtilisateur;
            choixUtilisateur = DemanderEtValiderChoixUtilisateur(lignesBottin, "Saisissez le numéro du site Web à lancer");

            //TODO Extraire l'URL selon le choix de l'utilisateur

            //TODO Démarrer le site Web demandé
            Console.ReadKey();
        }


        static void AfficherBottin(string[] lignesBottin)
        {
            Console.WriteLine("CONTENU DU BOTTIN WEB");
            Console.WriteLine("---------------------");

            for (int n=0; n<lignesBottin.Length; n++)
            {
                Console.WriteLine($"{n+1} - {lignesBottin[n].Split("|")[0]}");
            }
            Console.WriteLine("---------------------");
        }


        static string LireEmplacementBottin()
        {
            return Environment.CurrentDirectory + "\\bottin.txt" ;
        }

        static string[] LireContenuFichierBottin(string emplacementFichierBottin)
        {
            string[] lignes = new string[0];
            string ligneDeFichier;

            StreamReader fichier = new StreamReader(emplacementFichierBottin);
            while ((ligneDeFichier = fichier.ReadLine()) != null)
            {
                lignes = AjouterLigne(ligneDeFichier, lignes);
            }  
            fichier.Close();

            return lignes;
        }

        static int DemanderEtValiderChoixUtilisateur(string[] bottin, string messageInvite)
        {
            int choixUtilisateur;
            do
            {
                Console.WriteLine(messageInvite);
                choixUtilisateur =  int.Parse(Console.ReadLine());
            } while (choixUtilisateur < 1 || choixUtilisateur > bottin.Length);  
            return choixUtilisateur - 1;
        }


        static string[]  AjouterLigne (string nouvelleLigne, string[] tableauDestination)
        {
            tableauDestination = AgrandirTableau(tableauDestination);
            tableauDestination[tableauDestination.Length - 1] = nouvelleLigne;
            return tableauDestination;
        }

        static string[] AgrandirTableau(string[] tableauOriginal)
        {
            string[] resultat = new string[tableauOriginal.Length+1];
            resultat = CopierTableau(tableauOriginal, resultat);
            return resultat;
        }

        static string[] CopierTableau(string[] tableauSource, string[] tableauDestination)
        {
            for(int i=0; i < tableauSource.Length; i++)
            {
                tableauDestination[i] = tableauSource[i];
            }

            return tableauDestination;
        }
    }
}
